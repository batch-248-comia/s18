// console.log("Hello World");

//Functions
		// Functions are mosntly created for complicated task to run several lines code in succession.
		// Functions are also used to prevent repeating line/blocks of codes that perform the same task/functionality.
		
		let nickname = "Tolits";

		function printInput(){
			//let nickname = prompt("Enter your nickname:");
			console.log("Hi, " +nickname);
		}

		//printInput();

		// Consider this function
						//parameter
		function printName(name){
			console.log("My nickname is " +name);
		}

		//argument
		printName("Yen");
		printName("Mayen");

		// You can directly pass data into the function.

		//[SECTION] Parameters and Arguments

			//Parameter
				//"name" is called a parameter.
				//variable or container ONLY inside the function declared inside the ()
				// A "parameter" acts as a named variable that only exists inside a function.

			//Arguments
				// "Tolits" the information provided directly into the function is called an argument.
				// Values passed when invoking the function. 
				// functionName(argument);


		function checkDivisibilityBy8(num){
			let remainder = num % 8;
			console.log("The remainder of " + num + " divided by 8 is " +remainder);
			let isDivisibleBy8 = remainder === 0;
			console.log("Is " + num + " divisible by 8?");
			console.log(isDivisibleBy8);
		}

		checkDivisibilityBy8(64);
		checkDivisibilityBy8(25);

		/*
			mini-activity
			create a function that can check divisibility by 4

		*/

		function checkDivisibilityBy4(num){
			let remainder = num % 4;
			console.log("The remainder of " + num + " divided by 4 is " +remainder);
			let isDivisibleBy4 = remainder === 0;
			console.log("Is " + num + " divisible by 4?");
			console.log(isDivisibleBy4);
		}

		checkDivisibilityBy4(56);
		checkDivisibilityBy4(95);



// [SECTION] Function as Argument
	// Function Parameters can also accept other functions as arguments.
	// Some complex functions use other functions as arguments to perform more complicated result

		function argumentFunction(){
			console.log("This function was passed as an argument before the message was printed.")
		}

		function invokeFunction(argumentFunction){
			argumentFunction();
		}
		// Function used without a parenthesis is normally associated with using a FUNCTION as an ARGUMENT to another function
		invokeFunction(argumentFunction);
		// This is for fining information about a function in the console.log()
		console.log(argumentFunction);

//[SECTION] Using Multiple Parameters
	//Multiple "arguments" will correspond to the number of parameters declared in a function in succeeding order

		function createFullName(firstName, middleName, lastName, character){
			console.log(firstName + " " + middleName + " " + lastName + " is the " +character +".");
		}

		createFullName("Cardo","J.","Dalisay","Probinsyano");

		createFullName("Cardo","J.","Dalisay");

		createFullName("Cardo","J.","Dalisay","Probinsyano","Hello");

		// Use variables as Arguments

		let firstName = "Monkey";
		let middleName = "D.";
		let lastName = "Luffy";
		let character = "protagonist"

		createFullName(firstName, middleName, lastName, character);

		//the order of the argument is the same to the order of the parameters

		function printFullName(middleName, firstName, lastName){
			console.log(firstName + " " + middleName + " " + lastName);
		}

		printFullName("Carlo", "J.", "Caparas"); //J. Carlo Caparas

//Mini Activity

		function printFriends(friend1, friend2, friend3){
			console.log("My three friends are: " + friend1+ ", " + friend2+ ", " +friend3+ ".");
		}

		printFriends("Anna", "Elsa", "Olaf");


//[SECTION] Return Statement

		// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function

		function returnFullName(firstName, middleName, lastName){
			return firstName + " " + middleName + " " + lastName;
			console.log("This will not be printed.")
		}
		//return function ends the function statement
		// Whatever value is returned from the "returnFullName" function is now stored is now stored in the "completeName" variable
		let completeName = returnFullName("Karen", "L.", "Davila");

		console.log(completeName + " is my bestfriend.");
		console.log(completeName)

		console.log(returnFullName(firstName, middleName, lastName));



		function returnAddress(city, country){
			let fullAddress = city + ", "+ country;
			return fullAddress
		}

		let myAddress = returnAddress("Cebu City", "Philippines");
		console.log(myAddress);


		function printPlayerInfo(username, level, job){
			console.log("Username: " + username);
			console.log("Level: " + level);
			console.log("Job: " + job);

		};

		//When a function has only console.log() to display its result it will return undefined
		let user1 = printPlayerInfo("knight_blank",96,"Paladin");
		console.log(user1);//undefined

		// function promptName(name){
		// 	prompt("Please enter your name: ")

		// }
		
		function multiplyNumbers(num1, num2){
			return num1*num2
		}

		let product = multiplyNumbers(5,4);
		console.log("The product of 5 and 4 is ");
		console.log(product);
		
		